# semantic-release

- [Husky][husky]

[husky]: https://typicode.github.io/husky/#/

## Start project

```bash
npm init
```

## Install husky

```bash
npm install husky --save-dev
npx husky install
```

edit package.json and add prepare to scripts:

```json
...
"scripts": {
    "prepare": "husky install"
  },
...
```

## Install commitlint

```bash
npm install --save-dev @commitlint/{config-conventional,cli}
npm install husky --save-dev
npx husky install
npx husky add .husky/commit-msg "npx --no-install commitlint --edit $1"
```
